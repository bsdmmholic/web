$(document).ready(function(){
	var myFullpage = new fullpage('#fullpage', {
		anchors: ["firstPage","secondPage","3rdPage","4thPage","5thPage"],
		normalScrollElements:"#twitterBox",
		menu: "#fullMenu",
		slidesNavigation: true,
		css3: true,
		onLeave: function(origin, destination, direction){
			// origin.indx : 0~
			// direction : "down" / "up"
			var leavingSection = this;
			var destinationSection = destination.item;

			$(destinationSection).addClass("reveal");
		}
	});

	$(document).on("click","a.btnBgm",function(){
		var _this = $(this);
		if(_this.hasClass("on")){
			_this.removeClass("on");
			$("#bgmPlayer")[0].pause();
		}else{
			_this.addClass("on");
			$("#bgmPlayer")[0].play();
		}
	});

	$(".btnGoTop").click(function(e){
		e.preventDefault();
		myFullpage.moveTo(1);
	});
	

	$("#mainSection03 .hSlideBox").slick({
		dots: true,
		infinite:true,
		speed:500
	});
	$('#mainSection03 .hSlideBox').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$("#mainSection03 ul.character li").removeClass("active");
		$("#mainSection03 ul.character li").eq(nextSlide).addClass("active");

		var current = $("#mainSection03 .hSlideBox .slick-current");
		current.addClass("showOn");
		current.prev(".s-slider").addClass("showOn");
		current.next(".s-slider").addClass("showOn");
		current.next(".s-slider").next(".s-slider").addClass("showOn");
	});
	$('#mainSection03 .hSlideBox').on('afterChange', function(event, slick, currentSlide){
		var current = $("#mainSection03 .hSlideBox .slick-current");
		
		$("#mainSection03 .hSlideBox .s-slider").removeClass("showOn");
		current.addClass("showOn");
		current.next(".s-slider").addClass("showOn");
	});

	$("#mainSection04 .hSlideBox").slick({
		dots: false,
		infinite:true,
		speed:500,
		fade: true,
		swipe: false,
		cssEase:"linear"
	});
	$('#mainSection04 .hSlideBox').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var tabList = $("#mainSection04 .hSlideTab li");
		tabList.removeClass("active");
		tabList.eq(nextSlide).addClass("active");
	});

	$("#mainSection04 .hSlideTab li a").on("click",function(){
		var _this = $(this);
		var _li = _this.parent();
		var _idx = parseInt(_li.index());
		
		if(_li.hasClass("active")) return;

		_li.addClass("active");
		_li.siblings().removeClass("active");

		$("#mainSection04 .hSlideBox").slick("slickGoTo",_idx);
	});

	function makeThmPagination(){
		var _slider = $("#mainSection04 .hSlideBox .s-slider");
		_slider.each(function(){
			var wrapper = $(this).find(".charThms");
			var pageCnt = wrapper.find("> ul").length;

			var html = "";
			html += '<div class="pagination">'
			for(var i=1; i <= pageCnt; i++){
				if(i==1){
					html += '<span class="active"><a href="javascript:;">'+i+'</a></span>'
				}else{
					html += '<span><a href="javascript:;">'+i+'</a></span>'
				}
			}
			html += '</div>'

			wrapper.append(html);

		});
	};
	makeThmPagination();

	$(document).on("click",".charThms .pagination span a",function(e){
		e.preventDefault();

		var prt = $(this).parent();
		var paging = $(this).parents(".pagination");
		var _ul = paging.siblings("ul");
		var idx = prt.index();

		if(prt.hasClass("active")) return;

		prt.siblings().removeClass("active");
		prt.addClass("active");

		_ul.stop().hide();
		_ul.eq(idx).stop().fadeIn();
	});

	function mainAnimation(){
		var _util = $(".utilWrap");
		var aniTxt = $(".typingTxt ul.aniTxt");
		var txts = $(".typingTxt ul.aniTxt li");
		var txt02 = $(".typingTxt .txt02");
		var delArr = [300,450,600,900,1400,1550,1700,1850,2000,2200];
		
		txts.each(function(idx){
			var _txt = $(this).find("img");
			_txt.delay(delArr[idx]).fadeIn(50);
		});
		txt02.delay(2600).fadeIn();
		
		setTimeout(function(){
			_util.addClass("active");
		},200);
	}

	mainAnimation();
});
$(window).load(function(){
	
});
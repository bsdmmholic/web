var BGM = {
    SOUNDS: [],
    SOUND: null,
    btn_fnc: 'a#btn-sound',
    $btn_fnc: null,
    btn_slct: 'a.btn-slct',
    $btn_slct: null,
    cookie: {
        name: 'DC_MUSIC',
        expires: 7
    },
    volume: 0.75,
    current: 0,
    length: 0,
    init: function($slct) {
        this.$btn_fnc = $(this.btn_fnc);
        this.$btn_slct = $(this.btn_slct);
        $slct.each(function(i) {
            BGM.SOUNDS[i] = $slct.eq(i);
            BGM.SOUNDS[i][0].volume = BGM.volume;
        });
        this.length = BGM.SOUNDS.length;
        var loadedMeta = 0;
        for (var i = 0; i < BGM.SOUNDS.length; i++) {
            this.SOUNDS[i][0].addEventListener('loadedmetadata', function() {
                loadedMeta += 1;
            }, false);
        }
        var snd_int = setInterval(function() {
            if (loadedMeta === BGM.length) {
                clearInterval(snd_int);
                BGM.set();
            }
        }, 50);
    },
    set: function() {
        $('#header-BGM').addClass('is_BGM');
        this.SOUND = this.SOUNDS[this.current];
        if ($.cookie(this.cookie.name) === "OFF") {
            this.play(false);
        } else {
            this.play(true);
        }
        this.$btn_fnc.on('click', function() {
            if ($(this).hasClass('sound-on')) {
                BGM.play(false);
            } else {
                BGM.play(true);
            }
            return false;
        });
        this.$btn_slct.on('click', function() {
            var n = BGM.$btn_slct.index($(this));
            BGM.change(n);
            return false;
        });
    },
    change: function(n) {
        this.SOUND[0].pause();
        this.SOUND.off();
        setTimeout(function() {
            BGM.current = n;
            BGM.SOUND = BGM.SOUNDS[BGM.current];
            BGM.play(true);
        });
    },
    play: function(_bool) {
        if (_bool) {
            this.$btn_slct.each(function(i) {
                $(this).removeClass('current');
                if (i === BGM.current) $(this).addClass('current');
            });
            this.SOUND[0].play();
            this.SOUND[0].currentTime = 0;
            this.SOUND.on('ended.evtBGM', function() {
                BGM.SOUND.off();
                BGM.current = (BGM.current + 1 === BGM.length) ? 0 : BGM.current + 1;
                setTimeout(function() {
                    BGM.SOUND = BGM.SOUNDS[BGM.current];
                    BGM.play(true);
                });
            });
            this.$btn_fnc.removeClass('sound-off').addClass('sound-on');
            $.cookie(this.cookie.name, "ON", {
                expires: this.cookie.expires
            });
        } else {
            this.$btn_slct.each(function(i) {
                $(this).removeClass('current');
            });
            this.SOUND[0].pause();
            this.SOUND.off();
            this.$btn_fnc.removeClass('sound-on').addClass('sound-off');
            $.cookie(this.cookie.name, "OFF", {
                expires: this.cookie.expires
            });
        }
    }
};

$(function(){
	BGM.init($('.sound-BGM'));
});